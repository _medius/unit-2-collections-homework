1. My favorite approach in iterations is foreach because it much convenient to write and change it.
Stream API variant is readable in this case, but in more complex tasks it becomes more complicated
and difficult to write and change.
Using Iterator is cumbersome because it needs to create Iterator object and iterate over elements
by hands using next() method. Since java 5 better to use foreach construction. Undercut it unwrapped
to Iterator construction but you already don't need to make Iterator object and iterate by hands.

2. Code in main class in root directory.

3. Code in "queue" package.
In this case we need to synchronize the work of request generator an processors and we need to 
make "standby mode" for processors when queue is empty. BlockingQueue provides this functionality.
We can configure queue size and it makes sleep processor threads when queue is empty. But this
implementation is makes sleep RequestGenerator to when queue is overflowed. I think this situation
is unacceptable in real system and we need to adjust amount of processors or queue size to avoid
this situation.

4. Code "duplicates" package.
I can make it by three ways: place original collection into a set, using stream API command
distinct() and manually by iterate over collection. Code in 

5. Code of application used to measure performance in another project. Diagrams of the tests
in the root directory.
I want to compare HashMap and TreeMap by their performance.
Comparing how much memory allocates each of these two collections we can see that TreeMap doesn't
give significant advantage over HashMap. But it always using as much memory as it needs. HashMap 
allocate memory which not used to store objects when it expand hashtable.
In speed comparison we can see more interesting results. Basic operations (put, get, remove) in
HashMap performed by a constant time O(1) vs O(log n) time in TreeMap. And it difference is
significant very much on "cold" JVM. But after JIT-compiler working on "warmed up" VM speed test 
of TreeMap operations shows even better performance then HashMap which speed almost unchanged.

6. Code in "cache" package.
I implemented the simple LRU cache using LinkedHashMap standard method removeEldestEntry() and
set true to parameter accessOrder in constructor. In this case most long ago used element removed
from cache when we put new element.