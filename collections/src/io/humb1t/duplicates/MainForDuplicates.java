package io.humb1t.duplicates;

import java.util.*;
import java.util.stream.Collectors;

public class MainForDuplicates {
	private static final int COLLECTION_SIZE = 50;
	private static final int BOUND_FOR_RANDOM = 5;

	private static Collection<Integer> collection = new ArrayList<>();

	private static void createCollection() {
		for (int i = 0; i < COLLECTION_SIZE; i++) {
			collection.add(new Random().nextInt(BOUND_FOR_RANDOM));
		}
		System.out.println("Original collection\n" + collection + "\n");
	}

	public static void main(String[] args) {
		createCollection();

		Set<Integer> set = new HashSet<>(collection);
		System.out.println("Collection without duplicates via set\n" + set + "\n");

		List<Integer> listByStream = collection.stream()
				.distinct()
				.collect(Collectors.toList());
		System.out.println("Collection without duplicates via stream API\n" + listByStream + "\n");

		List<Integer> listByForEach = new ArrayList<>();
		for (Integer integer : collection) {
			if (!listByForEach.contains(integer)) {
				listByForEach.add(integer);
			}
		}
		System.out.println("Collection without duplicates via foreach\n" + listByForEach + "\n");
	}
}
