package io.humb1t.cache;

import java.util.LinkedHashMap;
import java.util.Map;

public class Cache<K, V> {
	private int cacheSize;

	@SuppressWarnings("unchecked")
	private Map<K, V> cache = new LinkedHashMap(cacheSize, 1f, true) {

		@Override
		protected boolean removeEldestEntry(Map.Entry eldest) {
			boolean cacheOverflow = cache.size() > cacheSize;
			if (cacheOverflow) {
				System.out.println("Remove eldest element from cache. Key "
						+ eldest.getKey() + " value " + eldest.getValue());
			}
			return cacheOverflow;
		}
	};

	public Cache(int cacheSize) {
		this.cacheSize = cacheSize;
	}

	public void putInto(K key, V value) {
		cache.put(key, value);
	}

	public V getFrom (K key) {
		return cache.get(key);
	}

	public int getCacheSize() {
		return cacheSize;
	}

	public void setCacheSize(int cacheSize) {
		this.cacheSize = cacheSize;
	}


}
