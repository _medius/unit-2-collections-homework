package io.humb1t.cache;

import java.util.HashMap;
import java.util.Map;

public class MainForCache {
	private static final int DATA_BOUND = 30;
	private static final int CACHE_SIZE = 4;

	private static Map<Integer, Integer> lowSpeedMemory = new HashMap<>();
	private static Cache<Integer, Integer> cache = new Cache<>(CACHE_SIZE);

	static {
		for (int i = 0; i < DATA_BOUND; i++) {
			lowSpeedMemory.put(i, i);
		}
	}

	private static Integer getValue(Integer key) {
		System.out.println("Request val by key " + key);
		Integer valueToReturn = cache.getFrom(key);

		if (valueToReturn != null) {
			System.out.println("Returned from cache\n");
			return valueToReturn;
		} else {
			System.out.println("Returned from low speed memory\n");
			valueToReturn = lowSpeedMemory.get(key);
			cache.putInto(key, valueToReturn);
			return valueToReturn;
		}
	}

	public static void main(String[] args) {
		for (int i = 0; i < 4; i++) {
			getValue(i);
			getValue(i);
		}

		getValue(0);
		getValue(0);

		getValue(1);
		getValue(1);

		for (int i = 4; i < 7; i++) {
			getValue(i);
			getValue(i);
		}
	}
}