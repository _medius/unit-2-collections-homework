package io.humb1t.queue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainForQueue {
	private static final int QUEUE_SIZE = 3;                //5
	private static final int GENERATOR_WORKING_TIME = 10;  //800 awaiting by processors
	private static final int PROCESSOR_WORKING_TIME = 800;  //100
	private static final int THREAD_POOL_SIZE = 4;
	private static final int NUMBER_OF_PROCESSORS = 3;

	private static ArrayBlockingQueue<Request> queue = new ArrayBlockingQueue<>(QUEUE_SIZE);

	public static void main(String[] args) {
		ExecutorService service = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
			for (int i = 0; i < NUMBER_OF_PROCESSORS; i++) {
				service.submit(new Processor(queue, PROCESSOR_WORKING_TIME));
			}
			service.submit(new RequestGenerator(queue, GENERATOR_WORKING_TIME));
			service.shutdown();
	}
}
