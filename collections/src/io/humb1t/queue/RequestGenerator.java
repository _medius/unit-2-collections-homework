package io.humb1t.queue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

public class RequestGenerator implements Runnable {
	private static final int NUMBER_OF_REQUESTS = 10;

	private final int workingTime;

	private ArrayBlockingQueue<Request> queue;

	public RequestGenerator(ArrayBlockingQueue<Request> queue, int workingTime) {
		this.workingTime = workingTime;
		this.queue = queue;
	}

	@Override
	public void run() {
		for (int i =0; i < NUMBER_OF_REQUESTS; i++) {
			try {
				TimeUnit.MILLISECONDS.sleep(workingTime);
				System.out.println("Generator try to put request.");
				queue.put(new Request());
				System.out.println("Generator has put request.");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
