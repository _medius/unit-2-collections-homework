package io.humb1t.queue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

public class Processor implements Runnable {
	private static int idSequence;

	private final int workingTime;
	private final int id;

	private ArrayBlockingQueue queue;

	public Processor(ArrayBlockingQueue queue, int workingTime) {
		this.workingTime = workingTime;
		this.queue = queue;
		this.id = idSequence++;
	}

	@Override
	public void run() {
		while (true) {
			try {
				System.out.println("Processor " + id + " try to take request.");
				queue.take();
				System.out.println("Processor " + id + " has taken request.");
				TimeUnit.MILLISECONDS.sleep(workingTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
