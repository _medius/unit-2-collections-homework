package io.humb1t;

import java.util.*;
import java.util.stream.Collectors;

public class Main {
	private static final int NUMBER_OF_ORDERS = 10;
	private static final int MAX_COST = 825;
	private static final int COST_BOUND = 200;

	public static void main(String[] args) {
		Collection<String> c = Collections.EMPTY_LIST;
		List<String> list = new ArrayList<>(c);

		List<Order> orders = Collections.singletonList(new Order(OrderStatus.COMPLETED));
		orders.stream()
				.filter(order -> order.status == OrderStatus.COMPLETED)
				.forEach(order -> System.out.println(order.toString()));
		for (Order order : orders) {
			System.out.println(order.toString());
		}
		for (Iterator<Order> iterator = orders.iterator(); iterator.hasNext();){
			System.out.println(iterator.next().toString());
		}
		Map<OrderStatus, List<Order>> ordersByStatus = orders.stream()
				.collect(Collectors.groupingBy(Order::getStatus));

		filterCollectionByNumericField();
	}

	public static void filterCollectionByNumericField() {
		List<Order> ordersList = new ArrayList<>();
		OrderStatus randomStatus;
		int randomCost;

		for (int i = 0; i < NUMBER_OF_ORDERS; i++) {
			randomStatus = OrderStatus.values()[new Random().nextInt(OrderStatus.values().length)];
			randomCost = new Random().nextInt(MAX_COST);

			ordersList.add(new Order(randomStatus, randomCost));
		}

		System.out.println("\nOrders with total cost over than " + COST_BOUND);

		System.out.println("\nStream API");
		ordersList.stream()
				.filter(order -> order.totalCost > COST_BOUND)
				.forEach(System.out::println);

		System.out.println("\nForEach");
		for (Order order : ordersList) {
			if (order.getTotalCost() > COST_BOUND) {
				System.out.println(order);
			}
		}

		System.out.println("\nIterator");
		Iterator<Order> iterator = ordersList.iterator();
		while (iterator.hasNext()) {
			Order order = iterator.next();
			if (order.getTotalCost() > COST_BOUND) {
				System.out.println(order);
			}
		}
	}

	public enum OrderStatus {
		NOT_STARTED, PROCESSING, COMPLETED
	}

	public static class Order {
		public final OrderStatus status;
		private int totalCost;

		public Order(OrderStatus status) {
			this.status = status;
		}

		public Order(OrderStatus status, int totalCost) {
			this.status = status;
			this.totalCost = totalCost;
		}

		public OrderStatus getStatus() {
			return status;
		}

		public int getTotalCost() {
			return totalCost;
		}

		public void setTotalCost(int totalCost) {
			this.totalCost = totalCost;
		}

		@Override
		public String toString() {
			return "Order{" +
					"status=" + status +
					", totalCost=" + totalCost +
					'}';
		}
	}
}
